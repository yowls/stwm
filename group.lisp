; -*- mode: lisp; -*-
(in-package :stumpwm)

;;   GROUPS
;;"""""""""""""""
;; aka workspaces

;; Groups name
(grename "1")
(gnewbg "2")
(gnewbg "3")
(gnewbg "4")
(gnewbg "5")
(gnewbg "6")
(gnewbg "7")
(gnewbg "8")
(gnewbg "9")
(gnewbg "0")


;; Switch focus to group
(define-key *top-map* (kbd "s-1") "gselect 1")
(define-key *top-map* (kbd "s-2") "gselect 2")
(define-key *top-map* (kbd "s-3") "gselect 3")
(define-key *top-map* (kbd "s-4") "gselect 4")
(define-key *top-map* (kbd "s-5") "gselect 5")
(define-key *top-map* (kbd "s-6") "gselect 6")
(define-key *top-map* (kbd "s-7") "gselect 7")
(define-key *top-map* (kbd "s-8") "gselect 8")
(define-key *top-map* (kbd "s-9") "gselect 9")
(define-key *top-map* (kbd "s-0") "gselect 0")


;; Switch focus to next/prev group
(define-key *top-map* (kbd "s-Right") "gnext")
(define-key *top-map* (kbd "s-Left") "gprev")


;; Move window and switch focus to group
(define-key *top-map* (kbd "s-!") "gmove-and-follow 1")
(define-key *top-map* (kbd "s-\"") "gmove-and-follow 2")
(define-key *top-map* (kbd "s-#") "gmove-and-follow 3")
(define-key *top-map* (kbd "s-$") "gmove-and-follow 4")
(define-key *top-map* (kbd "s-%") "gmove-and-follow 5")
(define-key *top-map* (kbd "s-&") "gmove-and-follow 6")
(define-key *top-map* (kbd "s-/") "gmove-and-follow 7")
(define-key *top-map* (kbd "s-(") "gmove-and-follow 8")
(define-key *top-map* (kbd "s-)") "gmove-and-follow 9")
(define-key *top-map* (kbd "s-=") "gmove-and-follow 0")
