#!/bin/bash

# Settings
DISPLAY_SIZE="1366x768"


# Launch Xephyr window
Xephyr -br -ac -noreset -screen $DISPLAY_SIZE :2 &
sleep 1

# Export display
export DISPLAY=:2
sleep 1


# Lauch window manager
stumpwm > ~/.cache/stump_log
