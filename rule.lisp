; -*- mode: lisp; -*-
(in-package :stumpwm)

;;   RULES
;;""""""""""""""""
;; Clear rules
(clear-window-placement-rules)

;; Mouse policy
(setq *mouse-focus-policy* :sloppy)

;;; Define window placement policy
;; ..


;; Frame preferences
; (define-frame-preference "Default"
;   (1 t nil :class "XTerm"))
;
; (define-frame-preference "Ardour"
;   (0 t   t   :title "Ardour - Session Control")
;   (0 nil nil :class "XTerm")
;   (1 t   nil :type :normal)
;   (0 t   t   :instance "ardour_editor" :type :normal)
;   (3 t   t   :instance "qjackctl" :role "qjackctlMainForm"))
;
; (define-frame-preference "Shareland"
;   (0 t   nil :class "XTerm")
;
; (define-frame-preference "Emacs"
;   (1 t t :restore "emacs-editing-dump" :title "...xdvi")
;   (0 t t :create "emacs-dump" :class "Emacs"))
