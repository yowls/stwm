; -*- mode: lisp; -*-
(in-package :stumpwm)

;;   WINDOW
;;"""""""""""""""
;; shortcuts to manipulate windows

;;;;;;;;;;;;;;;;;;;;;
;;;;;; STUMP ;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;

(define-key *top-map* (kbd "s-;") "colon")
(define-key *top-map* (kbd "s-:") "eval")
(define-key *top-map* (kbd "s-R") "restart-hard")
; (define-key *top-map* (kbd "s-d") "exec")

;; Get window list with prompt
(define-key *root-map* (kbd "w") "windowlist")


;;;;;;;;;;;;;;;;;;;;;
;;;;;; FOCUS ;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;

;; Focus with arrow keys
(define-key *top-map* (kbd "s-Right") "move-focus right")
(define-key *top-map* (kbd "s-Left") "move-focus left")
(define-key *top-map* (kbd "s-Up") "move-focus up")
(define-key *top-map* (kbd "s-Down") "move-focus down")

;; Focus with vim keybinds mode
(define-key *top-map* (kbd "s-h") "move-focus left")
(define-key *top-map* (kbd "s-j") "move-focus down")
(define-key *top-map* (kbd "s-k") "move-focus up")
(define-key *top-map* (kbd "s-l") "move-focus right")

;; Switch frame focus
(define-key *top-map* (kbd "s-Tab") "fnext")
; (define-key *top-map* (kbd "s-Tab") "fother")

;; Switch focus in current frame
(define-key *top-map* (kbd "M-Tab") "other-in-frame")
(define-key *top-map* (kbd "s-ISO_Left_Tab") "other-in-frame") ; ISO_Left is a “Shift” key

;; Kill window
(define-key *root-map* (kbd "q") "kill-window")

;; Fullscreen mode
(define-key *top-map* (kbd "s-C-f") "fullscreen")


;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;; MOVEMENT ;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;

;; Move window with arrow keys
;; (super + control + 'arrow')
(define-key *top-map* (kbd "s-C-Right") "move-window right")
(define-key *top-map* (kbd "s-C-Left") "move-window left")
(define-key *top-map* (kbd "s-C-Up") "move-window up")
(define-key *top-map* (kbd "s-C-Down") "move-window down")

;; Move window with vim-like keybinds
;; (super + control + 'h,j,k,l')
(define-key *top-map* (kbd "s-C-h") "move-window left")
(define-key *top-map* (kbd "s-C-j") "move-window down")
(define-key *top-map* (kbd "s-C-k") "move-window up")
(define-key *top-map* (kbd "s-C-l") "move-window right")

;; Hidden windows
(define-key *top-map* (kbd "s-n") "pull-hidden-next")
(define-key *top-map* (kbd "s-N") "pull-hidden-previous")

;; Exchange direction
(define-key *top-map* (kbd "s-m") "exchange-direction down")
(define-key *top-map* (kbd "s-M") "exchange-direction left")
(define-key *top-map* (kbd "s-<") "exchange-direction up")
(define-key *top-map* (kbd "s->") "exchange-direction right")


;;;;;;;;;;;;;;;;;;;;;;
;;;;;; RESIZE ;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;

;; (super + shift + 'h,j,k,l')
(define-key *top-map* (kbd "s-L") "resize 10 0")
(define-key *top-map* (kbd "s-K") "resize 0 10")


;;;;;;;;;;;;;;;;;;;;;;
;;;;;; FRAMES ;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;

(define-key *top-map* (kbd "s-i") "hsplit")
(define-key *top-map* (kbd "s-u") "vsplit")
(define-key *top-map* (kbd "s-r") "remove-split")
