; -*- mode: lisp; -*-
(in-package :stumpwm)

;;   KEYBINDS
;;"""""""""""""""

;; Special keys, to remember
;; s = super (mod4)
;; M = alt (mod1)
;; S = shift
;; C = control


;;;;;;;;;;;;;;;;;;;;;
;;;;;; STUMP ;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;

;; Prompt
(defcommand prompt (&optional (initial "")) (:rest)
  (let ((cmd (read-one-line (current-screen) ": " :initial-input initial)))
    (when cmd
      (eval-command cmd t))))

(define-key *root-map* (kbd "C-b") "prompt exec x-www-browser ")


;;;;;;;;;;;;;;;;;;;;;;
;;;;;; SYSTEM ;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;

;; NOTE: run-shell-command or exec?

;; Volume
(define-key *top-map* (kbd "XF86AudioRaiseVolume") "run-shell-command volume up")
(define-key *top-map* (kbd "XF86AudioLowerVolume") "run-shell-command volume down")
(define-key *top-map* (kbd "XF86AudioMute") "run-shell-command volume toggle")

;; Microphone
(define-key *top-map* (kbd "XF86AudioMicMute") "run-shell-command volume toggle-microphone")

;; Play
(define-key *top-map* (kbd "XF86AudioNext") "run-shell-command playerctl next")
(define-key *top-map* (kbd "XF86AudioPrev") "run-shell-command playerctl previous")
(define-key *top-map* (kbd "XF86AudioPlay") "run-shell-command playerctl play-pause")

;; Brightness
(define-key *top-map* (kbd "XF86MonBrightnessUp") "run-shell-command brightness up")
(define-key *top-map* (kbd "XF86MonBrightnessDown") "run-shell-command brightness down")

;; Screenshot
(define-key *top-map* (kbd "Print") "run-shell-command screenshot selective")
(define-key *top-map* (kbd "s-Print") "run-shell-command screenshot full")
(define-key *top-map* (kbd "M-Print") "run-shell-command screenshot partial")
(define-key *top-map* (kbd "M-S-Print") "run-shell-command screenshot floating")

;; Recording
; (define-key *top-map* (kbd "S-Print") "run-shell-command ..")

;; Session management
(define-key *root-map* (kbd "C-p") "run-shell-command rofi -show powermenu -modi powermenu:rofi-power-menu")

;; Lock screen
(define-key *root-map* (kbd "C-l") "exec xscreensaver-command -lock")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;; APPLICATIONS ;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Terminal
(define-key *top-map* (kbd "s-RET") "run-shell-command kitty")
(define-key *top-map* (kbd "C-M-t") "run-shell-command urxvt")

;; Text editor
(define-key *root-map* (kbd "C-v") "run-shell-command kitty -e nvim")
(define-key *root-map* (kbd "V") "run-shell-command atom")

;; File manager
(define-key *root-map* (kbd "C-f") "run-shell-command pcmanfm")
(define-key *root-map* (kbd "F") "run-shell-command nautilus")

;; Browser
(define-key *root-map* (kbd "C-b") "run-shell-command firefox")
(define-key *root-map* (kbd "B") "run-shell-command chromium")

;; Emacs
(define-key *root-map* (kbd "e") "run-shell-command emacs")
(define-key *root-map* (kbd "M-t") "run-shell-command emacs ~/MEGA/parrot.org")
(define-key *root-map* (kbd "M-e") "run-shell-command emacs ~/.emacs.d")
(define-key *root-map* (kbd "M-s") "run-shell-command emacs ~/.stumpwm.d")

;; Rofi launcher
(define-key *top-map* (kbd "M-space") "run-shell-command rofi -show drun")
(define-key *top-map* (kbd "M-x") "run-shell-command rofi -show run")
(define-key *top-map* (kbd "M-w") "run-shell-command rofi -show window")

;; Greenclip cliboard
(define-key *top-map* (kbd "M-c") "run-shell-command rofi -modi 'clipboard:greenclip print' -show clipboard -run-command '{cmd}'")
; (define-key *root-map* (kbd "S-c") "clear clipboard")

;; Others
(define-key *root-map* (kbd "C-ESC") "run-shell-command kitty -e htop")
(define-key *root-map* (kbd "T") "run-shell-command telegram-desktop")
(define-key *root-map* (kbd "D") "run-shell-command discord")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Web jump (works for Google and Imdb)
(defmacro make-web-jump (name prefix)
  `(defcommand ,(intern name) (search) ((:rest ,(concatenate 'string name " search: ")))
    (substitute #\+ #\Space search)
    (run-shell-command (concatenate 'string ,prefix search))))

(make-web-jump "google" "x-www-browser http://www.google.com/search?q=")

(define-key *root-map* (kbd "C-g") "google")
