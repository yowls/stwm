#!/usr/bin/env bash
# you can use ~/.xprofile to autostart on login

setxkbmap latam &
xsetroot -cursor_name left_ptr

# If it´s not running, Run it
run() { [ ! $(pgrep $1) ] && $@& }

#-- POLKIT | PAM | KEYRING
run /usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1
#run gnome-keyring

#-- LOCKSCREEN
run xscreensaver -nosplash

#-- COMPOSITOR
# run picom -b --experimental-backends --backend glx --vsync
run picom --config ~/.config/picom/picom.conf
run flashfocus
# run flashfocus -c ~/.config/flashfocus/flashfocus.yml

#-- NOTIFICATION
# run dunst
run dunst -config ~/.config/dunst/dunstrc

#-- STATUS BAR
~/.config/lemonbar/nunu/launch.sh
#run stalonetray

#-- Demons
run redshift
run greenclip daemon
#run urxvtd
#run emacs --daemon
#run pcmanfm --daemon-mode

#-- Custom Scripts
#run wal -R
#run dual_monitors

#-- WALLPAPER
~/.fehbg						# Auto feh script
# xwinwrap..					# Animated
# feh --no-fehbg --bg-fill ~/.stumpwm.d/imgs/wall.png		# Selective
