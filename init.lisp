;; -*-lisp-*-
;;;  ______     ______   __  __     __    __     ______
;;; /\  ___\   /\__  _\ /\ \/\ \   /\ "-./  \   /\  == \
;;; \ \___  \  \/_/\ \/ \ \ \_\ \  \ \ \-./\ \  \ \  _-/
;;;  \/\_____\    \ \_\  \ \_____\  \ \_\ \ \_\  \ \_\
;;;   \/_____/     \/_/   \/_____/   \/_/  \/_/   \/_/

(in-package :stumpwm)

;;;  SETTINGS
;;;"""""""""""""""
;; Prefix key
(set-prefix-key (kbd "s-space"))

;; Defaults programs
(defparameter *terminal* "kitty")
(defparameter *browser* "firefox")

;; Theme
(defparameter *theme* "one")


;;;  LOAD CONFIG
;;;"""""""""""""""
;; load config
; (load "~/.stumpwm.d/functions.lisp")
(load "~/.stumpwm.d/window.lisp")
(load "~/.stumpwm.d/group.lisp")
(load "~/.stumpwm.d/rule.lisp")
(load "~/.stumpwm.d/keybind.lisp")
(load "~/.stumpwm.d/Themes/one.lisp")

;; auto start programs
(run-shell-command "~/.fehbg")
; (run-shell-command "~/.stumpwm.d/autostart.sh")
