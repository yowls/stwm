#+CAPTION: Stump WM banner img
#+ATTR_ORG: :width 400px
#+ATTR_HTML: :width 400px :alt Stump img :title Stump banner img :align center
[[imgs/banner.png]]

* 📖 Description
[[http://stumpwm.github.io/][Stumpwm]] is a tiling window manager for X11.
The successor of Ratpoison. It's completely written and configured in Common Lisp.
Is very extensible and customizable, allowing to update the config while running.
It's a emacs-like window manager.

🪶 /version: 1.0.0/


*** 📝 Requirements:
+ xfontsel for choose the font
+ quicklisp


*Work In Progress..*
