; -*- mode: lisp; -*-
(in-package :stumpwm)


;; Start or focus a program
(defcommand xterm () ()
  "Start of focus xterm."
  (run-or-raise "xterm" '(:class "XTerm")))


;; Session menu
(defcommand my-power-menu () ()
  (let ((choice
         (select-from-menu (current-screen) '("shutdown" "restart" "quit") nil 0 nil)))
    (cond ((null choice)
           (throw 'error "Aborted."))
          ((string= choice "shutdown")
           (run-shell-command "poweroff"))
          ((string= choice "restart")
           (run-shell-command "reboot"))
          ((string= choice "quit")
           (quit)))))
