; -*- mode: lisp; -*-
(in-package :stumpwm)

;; Current Color Scheme :
;; Gruvbox-dark

(setf *colors*
      '("#2E3440"            ; black
        "#BF616A"            ; red
        "#A3BE8C"            ; green
        "#EBCB8B"            ; yellow
        "#5E81AC"            ; blue
        "#B48EAD"            ; magenta
        "#88C0D0"            ; cyan
        "#ECEFF4"            ; white
        "#A3BE8C"            ; spring-green
        "#D8DEE9"            ; gray9
        ))

(update-color-map (current-screen))

(defparameter *foreground-color* "#ECEFF4")
(defparameter *background-color* "#272C36")
(defparameter *border-color* "#5E81AC")

(setf *message-window-gravity* :center
      *input-window-gravity* :center
      *window-border-style* :thin
      *message-window-padding* 5
      *input-window-padding* 5)
(set-msg-border-width 4)
(set-fg-color *foreground-color*)
(set-bg-color *background-color*)
(set-border-color *border-color*)

(set-frame-outline-width 1)
(setf *normal-border-width* 2)
(setf *maxsize-border-width* 4)
(setf *transient-border-width* 2)
(set-focus-color *border-color*)
(set-unfocus-color *background-color*)


; (set-fg-color        (nth 7 *colors*))
; (set-bg-color        (nth 0 *colors*))
; (set-border-color    (nth 3 *colors*))
; (set-focus-color     (nth 3 *colors* ))
; (set-unfocus-color    (nth 8 *colors* ))
; (set-float-focus-color      (nth 3 *colors* ))
; (set-float-unfocus-color    (nth 8 *colors* ))

;; (setf *grab-pointer-foreground* (nth 7 *colors*))
;; (setf *grab-pointer-background* (nth 7 *colors*))
(setf *grab-pointer-character*         71 )
(setf *grab-pointer-character-mask*    71 )


;; (defcommand set-font-size (size) ()
  ;; "Set the font size of mode-line and stumwpm"
  ;; (set-font (make-instance 'xft:font :family "Essential PragmataPro" :subfamily "Regular" :size size)))
;;
;; (set-font-size 12)

;; Head gaps run along the 4 borders of the monitor(s)
;; (setf swm-gaps:*head-gaps-size* 0)
;; Inner gaps run along all the 4 borders of a window
;; (setf swm-gaps:*inner-gaps-size* 10)
;; Outer gaps add more padding to the outermost borders of a window (touching the screen border)
; (setf swm-gaps:*outer-gaps-size* 0)

;;(stumpwm::single-frame-p)

;; This function is undefined when using quicklisp's stumpwm package, causing crashes when executing toggle-gaps
;; It was copied from the stumpwm's master branch as a simple fix
;; source: https://github.com/stumpwm/stumpwm/blob/master/tile-window.lisp#L218
; (defun only-tile-windows (windows)
;   (remove-if-not (lambda (w) (typep w 'tile-window))
;                  windows))

;; (swm-gaps:toggle-gaps)
